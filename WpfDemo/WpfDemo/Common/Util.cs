﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfDemo.Common
{
    public static class Util
    {
        public static int GetAge(DateTime birthDate)
        {
            int _age = 0;
            int year = birthDate.Year;
            int byear = DateTime.Now.Year;
            _age = byear - year;

            int month = DateTime.Now.Month;
            int day = DateTime.Now.Day;

            int bmonth = birthDate.Date.Month;
            int bday = birthDate.Date.Day;

            if (month < bmonth)
            {
                _age = _age - 1;
            }
            else
            {
                if (month == bmonth)
                {
                    if (day < bday)
                    {
                        _age = _age - 1;
                    }
                }
            }
            return _age;
        }
    }
}
