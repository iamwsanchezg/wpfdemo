﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Window
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.WindowState = WindowState.Maximized;
        }

        private void ExitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void MathOperation_Click(object sender, RoutedEventArgs e)
        {
            MathOperation mo = new MathOperation();
            mo.Show();
        }

        private void TextEditor_Click(object sender, RoutedEventArgs e)
        {
            Notepad n = new Notepad();
            n.Show();
        }

        private void PersonWindow_Click(object sender, RoutedEventArgs e)
        {
            PersonWindow pw = new PersonWindow();
            pw.Show();
        }
    }
}
