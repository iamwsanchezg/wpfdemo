﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfDemo.Models;
using WpfDemo.Serialization;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for PersonWindow.xaml
    /// </summary>
    public partial class PersonWindow : Window
    {
        public PersonWindow()
        {
            InitializeComponent();
        }

        private void ToJSONButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                JsonSerialization.WriteToJsonFile("C:\\Personas\\person.json", GetDataFromForm());
                MessageBox.Show("¡Archivo creado correctamente!", "", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message,"",MessageBoxButton.OK,MessageBoxImage.Exclamation);
            }
        }

        private Person GetDataFromForm()
        {
            return new Person
            {
                FirstName = FirstNameTextBox.Text,
                MiddleName = MiddleNameTextBox.Text,
                LastName = LastNameTextBox.Text,
                BirthDate = BirthDateDatePicker.SelectedDate.Value
            };
        }

        private void ToXMLButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                XmlSerialization.WriteToXmlFile("C:\\Personas\\person.xml", GetDataFromForm());
                MessageBox.Show("¡Archivo creado correctamente!", "", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void ToBinaryButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BinarySerialization.WriteToBinaryFile("C:\\Personas\\person.bin", GetDataFromForm());
                MessageBox.Show("¡Archivo creado correctamente!", "", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }
    }
}
