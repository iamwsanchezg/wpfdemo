﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MathOperation.xaml
    /// </summary>
    public partial class MathOperation : Window
    {
        int mathOperator = 0;
        public MathOperation()
        {
            InitializeComponent();
        }

        private void CalculateButton_Click(object sender, RoutedEventArgs e)
        {
            decimal operator1 = 0;
            decimal operator2 = 0;
            decimal result = 0;
            bool c = false;
            try
            {
                c = decimal.TryParse(Operator1TextBox.Text, out operator1);
                c = decimal.TryParse(Operator2TextBox.Text, out operator2);

                switch (mathOperator)
                {
                    case 0:
                        result = operator1 + operator2;
                        break;
                    case 1:
                        result = operator1 - operator2;
                        break;
                    case 2:
                        result = operator1/operator2;
                        break;
                    case 3:
                        result = operator1 * operator2;
                        break;
                }
                ResultLabel.Content = result.ToString();
            }
            catch (Exception)
            {

            }
        }

        private void Checked_Handler(object sender, RoutedEventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            switch (rb.Name)
            {
                case "sum":
                    mathOperator = 0;
                    break;
                case "subtract":
                    mathOperator = 1;
                    break;
                case "division":
                    mathOperator = 2;
                    break;
                case "multiply":
                    mathOperator = 3;
                    break;
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ResultLabel.Content = "";
            Operator1TextBox.Text = "";
            Operator2TextBox.Text = "";
        }
    }
}
