﻿using Microsoft.Win32;
using System.Windows;
using WpfApp1.Common;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for Notepad.xaml
    /// </summary>
    public partial class Notepad : Window
    {
        public Notepad()
        {
            InitializeComponent();
        }

        private void SaveMenu_Click(object sender, RoutedEventArgs e)
        {
            // Configurar
            SaveFileDialog dlg = new SaveFileDialog
            {
                FileName = "Documento", // Nombre de archivo por defecto
                DefaultExt = ".txt", // Extensión por defecto
                Filter = "Archivos de texto (.txt)|*.txt" // filtro de archivos por extensión
            };

            // Mostrar el diálogo
            bool? result = dlg.ShowDialog();

            if (result == true)
            {
                System.IO.File.WriteAllText(dlg.FileName, TextUtilities.GetRichTextBoxText(editor));
            }
            
        }

        private void OpenMenu_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog
            {
                InitialDirectory = "c:\\",
                Filter = "Archivos de texto (*.txt)|*.txt|All Files (*.*)|*.*",
                RestoreDirectory = true
            };
            bool? result = dlg.ShowDialog();
            if (result==true)
            {
                TextUtilities.LoadTextDocument(dlg.FileName, editor);
            }
        }
    }
}
