﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfDemo.Common;

namespace WpfDemo.Models
{
    [Serializable]
    public class Person
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public int Age {
            get { return Util.GetAge(BirthDate); }
        }
        [NonSerialized]
        public string Comments = "";
    }
}
